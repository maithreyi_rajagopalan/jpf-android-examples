#!/bin/bash

echo "Cleaning environment."
ERR=`rm coverage.ec /tmp 2>&1`

echo "Building project."
#ERR=`ant clean build dist | tail -n2`
#echo $ERR

#JPF="$HOME/Development/JPF"

JPF_CORE="$JPF/jpf-core"
JPF_ANDROID="$JPF/jpf-android/jpf-android"
JPF_NHANDLER="$JPF/jpf-nhandler"
EMMA="$ANDROID_SDK/tools/lib/emma.jar"
CP="build/jpf-android-examples-examples.jar":"${JPF_ANDROID}/build/jpf-android.jar":"${JPF_NHANDLER}/build/jpf-nhandler.jar":"${JPF_CORE}/build/jpf.jar":"${EMMA}":"${JPF_ANDROID}/lib/android-stub.jar":"${JPF_ANDROID}/lib/android.jar":"$JPF_ANDROID/lib/bson-3.0.4.jar":"$JPF_ANDROID/lib/mongo-java-driver-3.1.0.jar":"$JPF_ANDROID/lib/xpp3_min-1.1.4c.jar":"$JPF_ANDROID/lib/xstream-1.4.8.jar":"lib/commons-io-2.5.jar":"$JPF_ANDROID/lib/jsqlparser.jar"  
TIME_START=$(date +%Y/%m/%d\ %H:%M:%S)
echo "CP: ${CP}"

echo "Started: ${TIME_START}."

java -Xmx256G -Xms128G -Dfile.encoding=UTF-8 -classpath "$CP" "$@"

TIME_END=$(date +%Y/%m/%d\ %H:%M:%S)
echo "Done: ${TIME_END}."

S=$'\n'
#alfred-ping  -msg "$1:${S}Started: ${TIME_START}${S}Done: ${TIME_END}."
