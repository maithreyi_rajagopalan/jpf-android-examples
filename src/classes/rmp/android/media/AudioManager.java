package android.media;

import gov.nasa.jpf.vm.AndroidVerify;
import android.os.Handler;
import android.os.Message;

public class AudioManager {

  /**
   * A failed focus change request.
   */
  public static final int AUDIOFOCUS_REQUEST_FAILED = 0;
  /**
   * A successful focus change request.
   */
  public static final int AUDIOFOCUS_REQUEST_GRANTED = 1;

  /**
   * @hide Used to indicate no audio focus has been gained or lost.
   */
  public static final int AUDIOFOCUS_NONE = 0;

  /**
   * Used to indicate a gain of audio focus, or a request of audio focus, of unknown duration.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN = 1;
  /**
   * Used to indicate a temporary gain or request of audio focus, anticipated to last a short amount of time.
   * Examples of temporary changes are the playback of driving directions, or an event notification.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN_TRANSIENT = 2;
  /**
   * Used to indicate a temporary request of audio focus, anticipated to last a short amount of time, and
   * where it is acceptable for other audio applications to keep playing after having lowered their output
   * level (also referred to as "ducking"). Examples of temporary changes are the playback of driving
   * directions where playback of music in the background is acceptable.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3;
  /**
   * Used to indicate a temporary request of audio focus, anticipated to last a short amount of time, during
   * which no other applications, or system components, should play anything. Examples of exclusive and
   * transient audio focus requests are voice memo recording and speech recognition, during which the system
   * shouldn't play any notifications, and media playback should have paused.
   * 
   * @see #requestAudioFocus(OnAudioFocusChangeListener, int, int)
   */
  public static final int AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE = 4;
  /**
   * Used to indicate a loss of audio focus of unknown duration.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   */
  public static final int AUDIOFOCUS_LOSS = -1 * AUDIOFOCUS_GAIN;
  /**
   * Used to indicate a transient loss of audio focus.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   */
  public static final int AUDIOFOCUS_LOSS_TRANSIENT = -1 * AUDIOFOCUS_GAIN_TRANSIENT;
  /**
   * Used to indicate a transient loss of audio focus where the loser of the audio focus can lower its output
   * volume if it wants to continue playing (also referred to as "ducking"), as the new focus owner doesn't
   * require others to be silent.
   * 
   * @see OnAudioFocusChangeListener#onAudioFocusChange(int)
   */
  public static final int AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK = -1 * AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK;

  public interface OnAudioFocusChangeListener {
    public void onAudioFocusChange(int focusChange);
  }

  public static final int STREAM_MUSIC = 0;
  private final InternalHandler sHandler;

  public AudioManager() {
    sHandler = new InternalHandler();

  }

  private static class InternalHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
      android.media.AudioManager.OnAudioFocusChangeListener result = (android.media.AudioManager.OnAudioFocusChangeListener) msg.obj;
      switch (msg.what) {
      case AudioManager.AUDIOFOCUS_GAIN:
        result.onAudioFocusChange(AudioManager.AUDIOFOCUS_GAIN);
        break;
      case AudioManager.AUDIOFOCUS_LOSS:
        result.onAudioFocusChange(AUDIOFOCUS_LOSS);
        break;
      case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
        result.onAudioFocusChange(AUDIOFOCUS_LOSS_TRANSIENT);
        break;
      case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
        result.onAudioFocusChange(AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK);
        break;
      }
    }
  }

  public int requestAudioFocus(android.media.AudioManager.OnAudioFocusChangeListener param0, int param1,
                               int param2) {
    Message message = null;
    int i = AndroidVerify.getInt(0, 3, "AudioManager.requestAudioFocus");
    switch (i) {
    case 0:
      message = sHandler.obtainMessage(AUDIOFOCUS_GAIN, param0);
      message.sendToTarget();
      return AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
    case 1:
      message = sHandler.obtainMessage(AUDIOFOCUS_LOSS, param0);
      break;
    case 2:
      message = sHandler.obtainMessage(AUDIOFOCUS_LOSS_TRANSIENT, param0);
      break;
    case 3:
      message = sHandler.obtainMessage(AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK, param0);
      break;
    }
    message.sendToTarget();
    return AudioManager.AUDIOFOCUS_REQUEST_FAILED;

  }

  public int abandonAudioFocus(android.media.AudioManager.OnAudioFocusChangeListener param0) {
    boolean b = AndroidVerify.getBoolean("AudioManager.abandonAudioFocus");
    if (b) {
      return AudioManager.AUDIOFOCUS_REQUEST_FAILED;
    } else {
      param0.onAudioFocusChange(AUDIOFOCUS_LOSS);
      return AUDIOFOCUS_REQUEST_GRANTED;
    }
  }
}