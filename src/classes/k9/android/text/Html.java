package android.text;
import gov.nasa.jpf.vm.Abstraction;

public class Html {
  public class TagHandler {

  }

  public class ImageGetter {

  }

  public static android.text.Html TOP = new android.text.Html();


  public Html(){
  }

  public static android.text.Spanned fromHtml(java.lang.String param0, android.text.Html.ImageGetter param1, android.text.Html.TagHandler param2){
    return ((android.text.Spanned)Abstraction.randomObject("android.text.Spanned"));
  }

  public static android.text.Spanned fromHtml(java.lang.String param0){
    return ((android.text.Spanned)Abstraction.randomObject("android.text.Spanned"));
  }
}