package android.text.util;
import gov.nasa.jpf.vm.Abstraction;

public class Rfc822Token {
  public static android.text.util.Rfc822Token TOP = new android.text.util.Rfc822Token();


  public Rfc822Token(){
  }

  public java.lang.String getAddress(){
    return Abstraction.TOP_STRING;
  }

  public java.lang.String getName(){
    return Abstraction.TOP_STRING;
  }
}