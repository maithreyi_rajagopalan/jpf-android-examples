package android.graphics.drawable;
import gov.nasa.jpf.vm.Abstraction;

public class ShapeDrawable extends android.graphics.drawable.Drawable {
  public static android.graphics.drawable.ShapeDrawable TOP = new android.graphics.drawable.ShapeDrawable();


  public ShapeDrawable(){
  }

  public ShapeDrawable(android.graphics.drawable.shapes.Shape param0){
  }

  public android.graphics.Paint getPaint(){
    return ((android.graphics.Paint)Abstraction.randomObject("android.graphics.Paint"));
  }
}