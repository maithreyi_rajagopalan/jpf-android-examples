package android.widget;

import gov.nasa.jpf.vm.Abstraction;
import android.content.Context;
import android.util.AttributeSet;

public class ExpandableListView extends android.widget.ListView {

  public class OnChildClickListener {

  }

  public ExpandableListView(Context context) {
    super(context);
  }

  public ExpandableListView(Context context, AttributeSet set) {
    super(context);
  }

  public ExpandableListView(android.content.Context param0, android.util.AttributeSet param1, int param2) {
    super(param0);
  }

  public void setOnChildClickListener(android.widget.ExpandableListView.OnChildClickListener param0) {
  }

  public boolean expandGroup(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean setSelectedChild(int param0, int param1, boolean param2) {
    return Abstraction.TOP_BOOL;
  }
}