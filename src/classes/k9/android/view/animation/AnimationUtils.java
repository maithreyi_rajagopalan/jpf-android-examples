package android.view.animation;
import gov.nasa.jpf.vm.Abstraction;

public class AnimationUtils {
  public static android.view.animation.AnimationUtils TOP = new android.view.animation.AnimationUtils();


  public AnimationUtils(){
  }

  public static android.view.animation.Animation loadAnimation(android.content.Context param0, int param1) throws android.content.res.Resources.NotFoundException {
    return ((android.view.animation.Animation)Abstraction.randomObject("android.view.animation.Animation"));
  }
}