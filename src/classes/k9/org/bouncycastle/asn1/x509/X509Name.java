package org.bouncycastle.asn1.x509;
import gov.nasa.jpf.vm.Abstraction;

public class X509Name {
  public static final org.bouncycastle.asn1.DERObjectIdentifier CN = ((org.bouncycastle.asn1.DERObjectIdentifier)Abstraction.randomObject("org.bouncycastle.asn1.DERObjectIdentifier"));
  public static org.bouncycastle.asn1.x509.X509Name TOP = new org.bouncycastle.asn1.x509.X509Name();


  public X509Name(){
  }

  public X509Name(java.lang.String param0){
  }

  public java.util.Vector getValues(){
    return ((java.util.Vector)Abstraction.randomObject("java.util.Vector"));
  }

  public java.util.Vector getOIDs(){
    return ((java.util.Vector)Abstraction.randomObject("java.util.Vector"));
  }
}