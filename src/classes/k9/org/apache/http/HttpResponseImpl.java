package org.apache.http;
import gov.nasa.jpf.vm.Abstraction;

public class HttpResponseImpl implements org.apache.http.HttpResponse {
  public static org.apache.http.HttpResponseImpl TOP = new org.apache.http.HttpResponseImpl();


  public org.apache.http.StatusLine getStatusLine(){
    return ((org.apache.http.StatusLine)Abstraction.randomObject("org.apache.http.StatusLine"));
  }

  public org.apache.http.HttpEntity getEntity(){
    return ((org.apache.http.HttpEntity)Abstraction.randomObject("org.apache.http.HttpEntity"));
  }

  public org.apache.http.Header getFirstHeader(java.lang.String param0){
    return ((org.apache.http.Header)Abstraction.randomObject("org.apache.http.Header"));
  }
}