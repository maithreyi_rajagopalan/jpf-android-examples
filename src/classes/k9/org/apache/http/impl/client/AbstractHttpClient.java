package org.apache.http.impl.client;
import gov.nasa.jpf.vm.Abstraction;

public class AbstractHttpClient {
  public static org.apache.http.impl.client.AbstractHttpClient TOP = new org.apache.http.impl.client.AbstractHttpClient();


  public AbstractHttpClient(){
  }

  public final synchronized org.apache.http.params.HttpParams getParams(){
    return ((org.apache.http.params.HttpParams)Abstraction.randomObject("org.apache.http.params.HttpParams"));
  }

  public final synchronized org.apache.http.conn.ClientConnectionManager getConnectionManager(){
    return ((org.apache.http.conn.ClientConnectionManager)Abstraction.randomObject("org.apache.http.conn.ClientConnectionManager"));
  }
}