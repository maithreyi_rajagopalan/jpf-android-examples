package org.apache.http.client;

public interface CookieStore {


  public abstract void clear();

  public abstract java.util.List getCookies();
}