package org.apache.james.mime4j.codec;

import java.io.IOException;

public class Base64InputStream extends java.io.InputStream {
  public static org.apache.james.mime4j.codec.Base64InputStream TOP = new org.apache.james.mime4j.codec.Base64InputStream();

  public Base64InputStream() {
  }

  public Base64InputStream(java.io.InputStream param0) {
  }

  @Override
  public int read() throws IOException {
    return 0;
  }
}