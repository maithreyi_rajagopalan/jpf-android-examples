package org.apache.james.mime4j.field.address.parser;
import gov.nasa.jpf.vm.Abstraction;

public class AddressBuilder {
  public static org.apache.james.mime4j.field.address.parser.AddressBuilder TOP = new org.apache.james.mime4j.field.address.parser.AddressBuilder();


  public AddressBuilder(){
  }

  public static org.apache.james.mime4j.dom.address.AddressList parseAddressList(java.lang.String param0) throws org.apache.james.mime4j.field.address.parser.ParseException {
    return ((org.apache.james.mime4j.dom.address.AddressList)Abstraction.randomObject("org.apache.james.mime4j.dom.address.AddressList"));
  }
}