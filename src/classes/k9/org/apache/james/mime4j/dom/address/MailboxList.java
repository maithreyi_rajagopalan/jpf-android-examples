package org.apache.james.mime4j.dom.address;
import gov.nasa.jpf.vm.Abstraction;

public class MailboxList extends java.util.AbstractList implements java.io.Serializable {
  public static org.apache.james.mime4j.dom.address.MailboxList TOP = new org.apache.james.mime4j.dom.address.MailboxList();


  public MailboxList(){
  }

  public int size(){
    return Abstraction.TOP_INT;
  }

  public org.apache.james.mime4j.dom.address.Mailbox get(int param0){
    return ((org.apache.james.mime4j.dom.address.Mailbox)Abstraction.randomObject("org.apache.james.mime4j.dom.address.Mailbox"));
  }
}