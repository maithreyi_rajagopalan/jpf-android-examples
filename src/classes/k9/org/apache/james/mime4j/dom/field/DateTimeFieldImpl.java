package org.apache.james.mime4j.dom.field;
import gov.nasa.jpf.vm.Abstraction;

public class DateTimeFieldImpl implements org.apache.james.mime4j.dom.field.DateTimeField {
  public static org.apache.james.mime4j.dom.field.DateTimeFieldImpl TOP = new org.apache.james.mime4j.dom.field.DateTimeFieldImpl();


  public java.util.Date getDate(){
    return ((java.util.Date)Abstraction.randomObject("java.util.Date"));
  }
}