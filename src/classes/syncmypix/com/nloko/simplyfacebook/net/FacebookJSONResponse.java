package com.nloko.simplyfacebook.net;

import gov.nasa.jpf.vm.Abstraction;


public class FacebookJSONResponse extends com.nloko.simplyfacebook.net.Response {
  public static com.nloko.simplyfacebook.net.FacebookJSONResponse TOP = new com.nloko.simplyfacebook.net.FacebookJSONResponse();

  public String data = "";

  public FacebookJSONResponse() {
  }

  public FacebookJSONResponse(int param0, java.lang.String param1) {
  }

  public boolean isError() {
    return Abstraction.TOP_BOOL;
  }
}