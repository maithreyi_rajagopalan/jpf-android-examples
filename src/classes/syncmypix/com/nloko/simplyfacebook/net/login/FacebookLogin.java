package com.nloko.simplyfacebook.net.login;

import gov.nasa.jpf.vm.AndroidVerify;

import java.net.MalformedURLException;
import java.net.URL;

public class FacebookLogin {
  public void setAPIKey(String key) {
  }

  public URL getNextUrl() throws MalformedURLException {
    boolean b = AndroidVerify.getBoolean("FacebookLogin.getFullLoginUrl() - success?");
    if (b) {
      return new URL("http://www.facebook.com/cancel");
    } else {
      return new URL("http://www.facebook.com/login");
    }
  }

  public URL getCancelUrl() throws MalformedURLException {
    return new URL("http://www.facebook.com/cancel");

  }

  

  public String getFullLoginUrl() {
    boolean b = AndroidVerify.getBoolean("FacebookLogin.getFullLoginUrl() - success?");
    if (b) {
      return "http://www.facebook.com/cancel";
    } else {
      return "http://www.facebook.com/login";
    }

  }

  public void setResponseFromExternalBrowser(URL url) {

  }


  public boolean isLoggedIn() {
    boolean b = AndroidVerify.getBoolean("FacebookLogin.getFullLoginUrl() - success?");
    return !b;
  }

  public String getSessionKey() {
    return "SessionKey";
  }

  public String getSecret() {
    return "Secret";

  }

  public String getUid() {
    return "Uid";

  }
}
