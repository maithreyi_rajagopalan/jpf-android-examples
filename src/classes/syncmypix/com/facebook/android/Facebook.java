package com.facebook.android;

import gov.nasa.jpf.vm.Abstraction;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class Facebook {
//  public static com.facebook.android.Facebook TOP = new com.facebook.android.Facebook("111");
  // Strings used in the authorization flow
  public static final String REDIRECT_URI = "fbconnect://success";
  public static final String CANCEL_URI = "fbconnect://cancel";
  public static final String TOKEN = "access_token";
  public static final String EXPIRES = "expires_in";
  public static final String SINGLE_SIGN_ON_DISABLED = "service_disabled";

  public static final int FORCE_DIALOG_AUTH = -1;

  public static final int SUCCESS = 1;
  public static final int ERROR_FB = 3;
  public static final int CANCEL = 4;
  public static final int ERROR = 2;
  private DialogListener mAuthDialogListener;

  private final InternalHandler sHandler = new InternalHandler();

  public Facebook(String appId) {
  }

  public java.lang.String request(java.lang.String param0) throws java.net.MalformedURLException,
      java.io.IOException {
    return Abstraction.TOP_STRING;
  }

  public java.lang.String request(android.os.Bundle param0) throws java.net.MalformedURLException,
      java.io.IOException {
    return Abstraction.TOP_STRING;
  }

  public void authorize(Activity activity, DialogListener listener) {
    int i = AndroidVerify.getInt(1, 4, "Facbeook.authorize() - fire listener");
    mAuthDialogListener = listener;

   
    Message m = null;
    if (i == 1) {
      m = sHandler.obtainMessage();
      m.what = SUCCESS;
      m.obj = new FacebookResult(mAuthDialogListener, new Object[] {});

    } else if (i == 2) {
      m = sHandler.obtainMessage();
      m.what = ERROR_FB;
      m.obj = new FacebookResult(mAuthDialogListener, new Object[] {});
    } else if (i == 3) {
      m = sHandler.obtainMessage();
      m.what = ERROR;
      m.obj = new FacebookResult(mAuthDialogListener, new Object[] {});
    } else {
      m = sHandler.obtainMessage();
      m.what = CANCEL;
      m.obj = new FacebookResult(mAuthDialogListener, new Object[] {});
    }

    sHandler.sendMessage(m);
    System.out.println("@@@@@Pushing message on queue");

  }

  public String logout(Context context) throws MalformedURLException, IOException {
    return "true";
  }

  /**
   * Retrieve the OAuth 2.0 access token for API access: treat with care. Returns null if no session exists.
   *
   * @return String - access token
   */
  public String getAccessToken() {
    return null;
  }

  /**
   * Callback interface for dialog requests.
   *
   */
  public static interface DialogListener {

    /**
     * Called when a dialog completes.
     *
     * Executed by the thread that initiated the dialog.
     *
     * @param values
     *          Key-value string pairs extracted from the response.
     */
    public void onComplete(Bundle values);

    /**
     * Called when a Facebook responds to a dialog with an error.
     *
     * Executed by the thread that initiated the dialog.
     *
     */
    public void onFacebookError(FacebookError e);

    /**
     * Called when a dialog has an error.
     *
     * Executed by the thread that initiated the dialog.
     *
     */
    public void onError(DialogError e);

    /**
     * Called when a dialog is canceled by the user.
     *
     * Executed by the thread that initiated the dialog.
     *
     */
    public void onCancel();

  }

  private static class InternalHandler extends Handler {
    @Override
    public void handleMessage(Message msg) {
      FacebookResult result = (FacebookResult) msg.obj;
      System.out.println("Facebook$InternalHandler " + msg.what);
      switch (msg.what) {
      case SUCCESS:
        // There is only one result
        result.mListener.onComplete(null);
        break;
      case ERROR:
        result.mListener.onError(null);
        break;
      case ERROR_FB:
        result.mListener.onFacebookError(null);
        break;
      case CANCEL:
        result.mListener.onCancel();
        break;

      }
    }
  }

  private static class FacebookResult {
    final DialogListener mListener;
    final Object[] mData;

    FacebookResult(DialogListener task, Object... data) {
      mListener = task;
      mData = data;
    }
  }

}