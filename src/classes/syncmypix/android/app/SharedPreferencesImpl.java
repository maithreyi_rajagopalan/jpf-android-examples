package android.app;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class SharedPreferencesImpl implements android.content.SharedPreferences {
  public final WeakHashMap<OnSharedPreferenceChangeListener, Object> mListeners = new WeakHashMap<OnSharedPreferenceChangeListener, Object>();

  public SharedPreferencesImpl(File f, int mode) {
  }

  public SharedPreferencesImpl() {
  }

  public boolean getBoolean(java.lang.String param0, boolean param1) {
    return !AndroidVerify.getBoolean("android.content.SharedPreferences.getBoolean()");
  }

  public long getLong(java.lang.String param0, long param1) {
    return param1;
  }

  @FilterField
  @NeverBreak
  EditorImpl eimpl = new EditorImpl();

  @Override
  public Editor edit() {
    return eimpl;
  }

  public void startReloadIfChangedUnexpectedly() {
    // do nothing
  }

  public int getInt(java.lang.String param0, int param1) {
    return param1;
  }

  @Override
  public Map<String, ?> getAll() {
    return null;
  }

  @Override
  public String getString(String key, String defValue) {
    if (key.equals("source"))
      return "com.nloko.android.syncmypix.facebook.FacebookSyncService";
    if (key.equals("session_key") || key.equals("secret") || key.equals("uid"))
      return !AndroidVerify.getBoolean("SyncMyPix.loggedIn") ? "12345678" : null;
    //      return "12345678";
    return defValue;
  }

  @Override
  public Set<String> getStringSet(String key, Set<String> defValues) {
    return defValues;
  }

  @Override
  public float getFloat(String key, float defValue) {
    return defValue;
  }

  @Override
  public boolean contains(String key) {
    return true;
  }

  @NeverBreak
  @FilterField
  private static final Object mContent = new Object();

  @Override
  public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.put(listener, mContent);
    }
  }

  @Override
  public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.remove(listener);
    }
  }

  public final class EditorImpl implements Editor {

    @Override
    public Editor putString(String key, String value) {
      return this;
    }

    @Override
    public Editor putStringSet(String key, Set<String> values) {
      return this;
    }

    @Override
    public Editor putInt(String key, int value) {
      return this;
    }

    @Override
    public Editor putLong(String key, long value) {
      return this;
    }

    @Override
    public Editor putFloat(String key, float value) {
      return this;
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
      return this;
    }

    @Override
    public Editor remove(String key) {
      return this;
    }

    @Override
    public Editor clear() {
      return this;
    }

    @Override
    public void apply() {
    }

    @Override
    public boolean commit() {
      return true;
    }
  }

  @Override
  public Map<OnSharedPreferenceChangeListener, Object> getListeners() {
    return mListeners;
  }

}