package org.apache.http.client;

public class HttpClientImpl implements org.apache.http.client.HttpClient {
  public static final org.apache.http.client.HttpClientImpl TOP = new org.apache.http.client.HttpClientImpl();


  public org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest param0){
    return org.apache.http.HttpResponseImpl.TOP;
  }

  public org.apache.http.conn.ClientConnectionManager getConnectionManager(){
    return org.apache.http.conn.ClientConnectionManagerImpl.TOP;
  }
}