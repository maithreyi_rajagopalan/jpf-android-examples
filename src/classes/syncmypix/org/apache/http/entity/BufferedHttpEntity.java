package org.apache.http.entity;

import java.io.ByteArrayInputStream;

public class BufferedHttpEntity {
  public static final org.apache.http.entity.BufferedHttpEntity TOP = new org.apache.http.entity.BufferedHttpEntity();


  public BufferedHttpEntity(){
  }

  public BufferedHttpEntity(org.apache.http.HttpEntity param0) throws java.io.IOException {
  }

  public java.io.InputStream getContent() throws java.io.IOException {
    return new ByteArrayInputStream(new byte[] { 0 });
  }

}