package org.json;

import gov.nasa.jpf.vm.Abstraction;

public class JSONObject {
  public static final org.json.JSONObject TOP = new org.json.JSONObject();


  public JSONObject(){
  }

  public JSONObject(java.lang.String param0) throws org.json.JSONException {
  }

  public org.json.JSONArray getJSONArray(java.lang.String param0) throws org.json.JSONException {
    return org.json.JSONArray.TOP;
  }

  public java.lang.String getString(java.lang.String param0) throws org.json.JSONException {
    return Abstraction.TOP_STRING;
  }

  public JSONObject put(String s, Object o) {
    return TOP;

  }
}