package org.json;

import gov.nasa.jpf.vm.Abstraction;


public class JSONArray {
  public static final org.json.JSONArray TOP = new org.json.JSONArray();

  public JSONArray() {
  }

  public java.lang.Object get(int param0) throws org.json.JSONException {
    return Abstraction.TOP_STRING;
  }

  public int length() {
    return 2;
  }

  public JSONArray(java.lang.String param0) throws org.json.JSONException {
  }

  public org.json.JSONObject getJSONObject(int param0) throws org.json.JSONException {
    return org.json.JSONObject.TOP;
  }
}