package android.app;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class SharedPreferencesImpl implements android.content.SharedPreferences {
  public final WeakHashMap<OnSharedPreferenceChangeListener, Object> mListeners = new WeakHashMap<OnSharedPreferenceChangeListener, Object>();

  public SharedPreferencesImpl(File f, int mode) {
  }

  public SharedPreferencesImpl() {
  }

  public int getInt(java.lang.String param0, int param1) {
    return (int) AndroidVerify.getValues(new Object[] { 1, 3 }, "SharedPreferences.getFloat(" + param0 + ")");
  }

  public boolean getBoolean(java.lang.String param0, boolean param1) {
    return AndroidVerify.getBoolean("SharedPreferences.getBoolean(" + param0 + "");
  }

  public java.lang.String getString(java.lang.String param0, java.lang.String param1) {
    return AndroidVerify.getString(new String[] { "round_tip", "round_total" },
        "SharedPreferences.getString(" + param0 + ")");
  }

  public float getFloat(java.lang.String param0, float param1) {
    return (float) AndroidVerify.getValues(new Object[] { 0.0f, 53.2f }, "SharedPreferences.getFloat("
        + param0 + ")");
  }

  @Override
  public long getLong(String param0, long param1) {
    return param1;
  }

  public void startReloadIfChangedUnexpectedly() {
    // do nothing
  }

  @FilterField
  private static final Object mContent = new Object();

  @Override
  public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.put(listener, mContent);
    }

  }

  @Override
  public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.remove(listener);
    }
  }

  public final class EditorImpl implements Editor {

    @Override
    public Editor putString(String key, String value) {
      return this;
    }

    @Override
    public Editor putStringSet(String key, Set<String> values) {
      return this;
    }

    @Override
    public Editor putInt(String key, int value) {
      return this;
    }

    @Override
    public Editor putLong(String key, long value) {
      return this;
    }

    @Override
    public Editor putFloat(String key, float value) {
      return this;
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
      return this;
    }

    @Override
    public Editor remove(String key) {
      return this;
    }

    @Override
    public Editor clear() {
      return this;
    }

    @Override
    public void apply() {
    }

    @Override
    public boolean commit() {
      return true;

    }
  }

  @Override
  public Map<String, ?> getAll() {
    return null;
  }

  @Override
  public Set<String> getStringSet(String key, Set<String> defValues) {
    return defValues;
  }

  @Override
  public boolean contains(String key) {
    return true;
  }

  @FilterField
  @NeverBreak
  EditorImpl eimpl = new EditorImpl();

  @Override
  public Editor edit() {
    return eimpl;
  }

  @Override
  public Map<OnSharedPreferenceChangeListener, Object> getListeners() {
    return mListeners;
  }
}