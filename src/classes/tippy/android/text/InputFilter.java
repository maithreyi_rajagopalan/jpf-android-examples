package android.text;

public interface InputFilter {
  public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend);
}