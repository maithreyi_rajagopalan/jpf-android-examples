package android.database.sqlite;

import gov.nasa.jpf.vm.AndroidVerify;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;

public class SQLiteDatabase {
  private final String TAG = SQLiteDatabase.this.getClass().getSimpleName();
  private static final int SELECT = 0;
  private static final int INSERT = 1;
  private static final int UPDATE = 2;

  public class CursorFactory {

  }

  public long insertOrThrow(String tableName, String nullColumnHack, ContentValues values)
      throws SQLException {
    boolean b = AndroidVerify.getBoolean("SQLiteDatabase.query1");
    if (b) {
      return 2;
    } else {
      throw new SQLException("Can not read from DB");
    }
  }

  public long insert(String tableName, String nullColumnHack, ContentValues values) {
    boolean b1 = AndroidVerify.getBoolean("SQLiteDatabase.query2");
    if (b1) {
      return 2;
    } else {
      throw new SQLException("Can not read from DB2");
    }
  }

  public Cursor query(String tableName, String[] columns, String selection, String[] selectionArgs,
                      String groupBy, String having, String orderBy) throws Exception {

    // if the table does not exists
    if (tableName.equals("rssfeed")) {
      boolean b2 = AndroidVerify.getBoolean("SQLiteDatabase.query3");
      if (b2) {
        return new SQLiteCursor();
      } else {
        throw new Exception("Can not read from DB");
      }
    } else {
      boolean b2 = AndroidVerify.getBoolean("SQLiteDatabase.query4");
      if (b2) {
        return new SQLiteCursor();
      } else {
        throw new Exception("Can not read from DB");
      }
    }

  }

  public void execSQL(String string) throws Exception {
    boolean b3 = AndroidVerify.getBoolean("SQLiteDatabase.query5");
    if (!b3) {
      throw new Exception("Can not read from DB");
    }
  }

  public void delete(String tableRssitem, Object object, Object object2) {

  }

}
