package android.database.sqlite;

import android.database.DefaultCursor;

public class SQLiteCursor extends DefaultCursor {

  @Override
  public int getColumnIndex(String columnName) {
    switch (columnName) {
    case "title":
      return 1;
    case "link":
      return 3;
    case "pub_date":
      return 4;
    case "last_time_inserted":
      return 5;
    default:
      break;
    }
    return 0;
  }

  @Override
  public String getColumnName(int columnIndex) {
    switch (columnIndex) {
    case 1:
      return "title";
    case 3:
      return "link";
    case 4:
      return "pub_date";
    case 5:
      return "last_time_inserted";
    default:
      break;
    }
    return "";
  }

  @Override
  public String[] getColumnNames() {
    return new String[] { "_id", "title", "description", "link", "pub_date", "feed_id" };
  }

  @Override
  public String getString(int columnIndex) {
    if (columnIndex == 1) {
      return "Title";
    } else {
      return "http://feeds.feedburner.com/Mobilecrunch.rss";
    }
  }

}
