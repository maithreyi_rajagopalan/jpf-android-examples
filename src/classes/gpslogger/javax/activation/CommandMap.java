package javax.activation;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class CommandMap {
  public static javax.activation.CommandMap TOP = new javax.activation.CommandMap();


  public CommandMap(){
  }

  public static javax.activation.CommandMap getDefaultCommandMap(){
    return ((javax.activation.CommandMap)Abstraction.randomObject("javax.activation.CommandMap"));
  }

  public static void setDefaultCommandMap(javax.activation.CommandMap param0){
  }
}