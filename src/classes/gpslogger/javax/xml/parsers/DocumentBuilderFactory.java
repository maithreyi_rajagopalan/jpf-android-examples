package javax.xml.parsers;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class DocumentBuilderFactory {
  public static javax.xml.parsers.DocumentBuilderFactory TOP = new javax.xml.parsers.DocumentBuilderFactory();


  public DocumentBuilderFactory(){
  }

  public static javax.xml.parsers.DocumentBuilderFactory newInstance(){
    return ((javax.xml.parsers.DocumentBuilderFactory)Abstraction.randomObject("javax.xml.parsers.DocumentBuilderFactory"));
  }

  public javax.xml.parsers.DocumentBuilder newDocumentBuilder() throws javax.xml.parsers.ParserConfigurationException {
    return ((javax.xml.parsers.DocumentBuilder)Abstraction.randomObject("javax.xml.parsers.DocumentBuilder"));
  }
}