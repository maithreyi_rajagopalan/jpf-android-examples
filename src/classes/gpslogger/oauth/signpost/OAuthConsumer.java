package oauth.signpost;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public interface OAuthConsumer {


  public abstract java.lang.String getToken();

  public abstract java.lang.String getTokenSecret();

  public abstract void setTokenWithSecret(java.lang.String param0, java.lang.String param1);
}