package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public interface Document extends org.w3c.dom.Node {


  public abstract org.w3c.dom.NodeList getElementsByTagName(java.lang.String param0);

  public abstract org.w3c.dom.Text createTextNode(java.lang.String param0);

  public abstract org.w3c.dom.Element createElement(java.lang.String param0) throws org.w3c.dom.DOMException ;

  public abstract org.w3c.dom.Attr createAttribute(java.lang.String param0) throws org.w3c.dom.DOMException ;

  public abstract org.w3c.dom.Node getFirstChild();

  public abstract org.w3c.dom.Node appendChild(org.w3c.dom.Node param0) throws org.w3c.dom.DOMException ;

  public abstract java.lang.String getNodeValue() throws org.w3c.dom.DOMException ;

  public abstract org.w3c.dom.Node removeChild(org.w3c.dom.Node param0) throws org.w3c.dom.DOMException ;

  public abstract short getNodeType();

  public abstract org.w3c.dom.NamedNodeMap getAttributes();

  public abstract java.lang.String getNodeName();

  public abstract org.w3c.dom.NodeList getChildNodes();
}