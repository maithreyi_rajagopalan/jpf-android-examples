package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class NamedNodeMapImpl implements org.w3c.dom.NamedNodeMap {
  public static org.w3c.dom.NamedNodeMapImpl TOP = new org.w3c.dom.NamedNodeMapImpl();


  public org.w3c.dom.Node item(int param0){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public int getLength(){
    return Abstraction.TOP_INT;
  }
}