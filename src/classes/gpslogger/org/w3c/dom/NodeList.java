package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public interface NodeList {


  public abstract org.w3c.dom.Node item(int param0);

  public abstract int getLength();
}