package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class AttrImpl implements org.w3c.dom.Attr {
  public static org.w3c.dom.AttrImpl TOP = new org.w3c.dom.AttrImpl();


  public void setValue(java.lang.String param0){
  }

  public org.w3c.dom.Node getFirstChild(){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public org.w3c.dom.Node appendChild(org.w3c.dom.Node param0){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public java.lang.String getNodeValue(){
    return Abstraction.TOP_STRING;
  }

  public org.w3c.dom.Node removeChild(org.w3c.dom.Node param0){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public short getNodeType(){
    return Abstraction.TOP_SHORT;
  }

  public org.w3c.dom.NamedNodeMap getAttributes(){
    return ((org.w3c.dom.NamedNodeMap)Abstraction.randomObject("org.w3c.dom.NamedNodeMap"));
  }

  public java.lang.String getNodeName(){
    return Abstraction.TOP_STRING;
  }

  public org.w3c.dom.NodeList getChildNodes(){
    return ((org.w3c.dom.NodeList)Abstraction.randomObject("org.w3c.dom.NodeList"));
  }
}