package android.os;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.File;

public class Environment {

  @FilterField
  public static final java.lang.String MEDIA_UNKNOWN = "unknown";
  @FilterField
  public static final java.lang.String MEDIA_REMOVED = "removed";
  @FilterField
  public static final java.lang.String MEDIA_UNMOUNTED = "unmounted";
  @FilterField
  public static final java.lang.String MEDIA_CHECKING = "checking";
  @FilterField
  public static final java.lang.String MEDIA_NOFS = "nofs";
  @FilterField
  public static final java.lang.String MEDIA_MOUNTED = "mounted";
  @FilterField
  public static final java.lang.String MEDIA_MOUNTED_READ_ONLY = "mounted_ro";
  @FilterField
  public static final java.lang.String MEDIA_SHARED = "shared";
  @FilterField
  public static final java.lang.String MEDIA_BAD_REMOVAL = "bad_removal";
  @FilterField
  public static final java.lang.String MEDIA_UNMOUNTABLE = "unmountable";

  public Environment() {
  }

  private static final String[] states = { Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY,
    Environment.MEDIA_UNMOUNTED, Environment.MEDIA_SHARED };

  public static java.lang.String getExternalStorageState() {
    return AndroidVerify.getString(states, "Environment.getExternalStorageState");
  }

  @FilterField
  @NeverBreak
  private static final File f = new File("/storage/sdcard");

  public File getExternalStorageDirectory() {
    return f;
  }

}