package android.view;

import android.os.Parcel;
import android.os.Parcelable;
import gov.nasa.jpf.vm.Abstraction;

public final class MotionEvent {
  public static android.view.MotionEvent TOP = new android.view.MotionEvent();

  public MotionEvent() {
  }

  public final int getAction() {
    return Abstraction.TOP_INT;
  }

  public final float getX() {
    return Abstraction.TOP_FLOAT;
  }

  public final float getRawX() {
    return Abstraction.TOP_FLOAT;
  }

  public static final Parcelable.Creator<MotionEvent> CREATOR = new Parcelable.Creator<MotionEvent>() {
    public MotionEvent createFromParcel(Parcel in) {
      in.readInt(); // skip token, we already know this is a MotionEvent
      return MotionEvent.createFromParcelBody(in);
    }

    public MotionEvent[] newArray(int size) {
      return new MotionEvent[size];
    }
  };

  /** @hide */
  public static MotionEvent createFromParcelBody(Parcel in) {
    MotionEvent ev = new MotionEvent();
    return ev;
  }
}