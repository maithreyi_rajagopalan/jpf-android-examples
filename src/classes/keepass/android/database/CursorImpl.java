package android.database;

import gov.nasa.jpf.vm.Abstraction;

public class CursorImpl extends DefaultCursor {

 
  @Override
  public int getColumnIndexOrThrow(java.lang.String param0) {
    if (param0.equals("keyFile")) {
      return 0;
    } else if (param0.equals("fileName")) {
      return 1;
    }
    return Abstraction.TOP_INT;
  }

  @Override
  public java.lang.String getString(int param0) {
    if (param0 == 0) {
      return "keepass.kdb";
    } else if (param0 == 1) {
      return "file://keepass.kdb";
    }
    return Abstraction.TOP_STRING;
  }
}