package android.text.method;

public class ArrowKeyMovementMethod implements android.text.method.MovementMethod {
  public static android.text.method.ArrowKeyMovementMethod TOP = new android.text.method.ArrowKeyMovementMethod();


  public ArrowKeyMovementMethod(){
  }

  public static android.text.method.MovementMethod getInstance(){
    return android.text.method.ArrowKeyMovementMethod.TOP;
  }
}