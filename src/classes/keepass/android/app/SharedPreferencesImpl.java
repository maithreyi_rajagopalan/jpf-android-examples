package android.app;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class SharedPreferencesImpl implements android.content.SharedPreferences {

  public final WeakHashMap<OnSharedPreferenceChangeListener, Object> mListeners = new WeakHashMap<OnSharedPreferenceChangeListener, Object>();

  @FilterField
  @NeverBreak
  private static final Object mContent = new Object();

  public SharedPreferencesImpl() {
  }

  public SharedPreferencesImpl(File file, int mode) {
  }

  @Override
  public Map<String, ?> getAll() {
    return null;
  }

  @Override
  public Set<String> getStringSet(String key, Set<String> defValues) {
    return defValues;
  }

  @Override
  public int getInt(String key, int defValue) {
    return defValue;
  }

  @Override
  public long getLong(String key, long defValue) {
    return defValue;
  }

  @Override
  public float getFloat(String key, float defValue) {
    return defValue;
  }

  @Override
  public java.lang.String getString(String key, String defValue) {
    synchronized (this) {
      if (key.equals("defaultFileName")) {
        return "keepass.kdb";
        // return AndroidVerify.getString(new String[] { "keepass.kdb", "" },
        // "SharedPreferences.getString(defaultFileName)");
      } else if (key.equals("clip_timeout_key") || key.equals("app_timeout_key")) {
        return "300000";
      } else if (key.equals("list_size")) {
        return "2";
      }

      return defValue;
    }
  }

  @Override
  public boolean getBoolean(String key, boolean defValue) {
    if (key.equals("sort_key")) {
      return AndroidVerify.getBoolean("SharedPref(" + "sort_key" + ")");
    } else if (key.equals("omitbackup")) {
      return AndroidVerify.getBoolean("SharedPref(" + "omitbackup" + ")");
    } else if (key.equals("keyfile")) {
      return AndroidVerify.getBoolean("SharedPref(" + "keyfile" + ")");
    } else if (key.equals("maskpass")) {
      return AndroidVerify.getBoolean("SharedPref(" + "maskpass" + ")");
    }
    return defValue;
  }

  public void startReloadIfChangedUnexpectedly() {
    // do nothing
  }

  @Override
  public boolean contains(String key) {
    return true;
  }

  @FilterField
  @NeverBreak
  EditorImpl eimpl = new EditorImpl();

  @Override
  public Editor edit() {
    return eimpl;
  }

  @Override
  public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.put(listener, mContent);
    }

  }

  @Override
  public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.remove(listener);
    }
  }

  public final class EditorImpl implements Editor {

    @Override
    public Editor putString(String key, String value) {
      return this;
    }

    @Override
    public Editor putStringSet(String key, Set<String> values) {
      return this;
    }

    @Override
    public Editor putInt(String key, int value) {
      return this;
    }

    @Override
    public Editor putLong(String key, long value) {
      return this;
    }

    @Override
    public Editor putFloat(String key, float value) {
      return this;
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
      return this;
    }

    @Override
    public Editor remove(String key) {
      return this;
    }

    @Override
    public Editor clear() {
      return this;
    }

    @Override
    public boolean commit() {
      return true;
    }

    @Override
    public void apply() {
    }
  }

  @Override
  public Map<OnSharedPreferenceChangeListener, Object> getListeners() {
    return mListeners;
  }
}