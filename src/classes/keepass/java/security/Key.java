package java.security;

public interface Key {


  public abstract byte[] getEncoded();
}