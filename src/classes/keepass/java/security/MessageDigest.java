package java.security;

public class MessageDigest {
  public static java.security.MessageDigest TOP = new java.security.MessageDigest();

  String algorithm;
  int id; // set by native peer
  public MessageDigest(){
  }

  public static java.security.MessageDigest getInstance(java.lang.String param0) throws java.security.NoSuchAlgorithmException {
    return java.security.MessageDigest.TOP;
  }

  public byte[] digest(byte[] param0){
    return new byte[32];
  }

  public byte[] digest(){
    return new byte[32];
  }

  public void update(byte[] param0, int param1, int param2){
  }

  public void update(byte[] param0){
  }
}