package java.util;

public class Calendar {

  public static final Calendar TOP = new Calendar();

  public Calendar() {

  }

  public static Calendar getInstance() {
    return TOP;
  }

  public Calendar getInstance(TimeZone tz, Locale l) {
    return TOP;
  }

  public void setTimeZone(TimeZone tz) {

  }

  public void set(int field, int value) {

  }

  public Date getTime() {
    return new Date();
  }

  public void setTime(Date date) {

  }

  public int get(int i) {
    return 1;
  }
}
