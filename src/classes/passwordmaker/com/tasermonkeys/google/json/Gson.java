package com.tasermonkeys.google.json;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.vm.Abstraction;

import java.util.HashMap;
import java.util.Map;

public final class Gson {

  @FilterField
  static Map<String, Object> values = new HashMap<String, Object>();

  public Gson() {
  }

  public java.lang.String toJson(java.lang.Object param0) {
    values.put(param0.getClass().getName(), param0);
    return Abstraction.TOP_STRING;
  }

  public java.lang.Object fromJson(java.io.Reader param0, java.lang.Class param1) throws com.tasermonkeys.google.json.JsonSyntaxException,
      com.tasermonkeys.google.json.JsonIOException {
    Object o = values.get(param1.getName());

    return o;
  }
}