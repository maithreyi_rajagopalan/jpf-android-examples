package com.tasermonkeys.google.json;

public final class GsonBuilder {

  public GsonBuilder() {
  }

  public com.tasermonkeys.google.json.Gson create() {
    return new Gson();
  }

  public com.tasermonkeys.google.json.GsonBuilder registerTypeAdapter(java.lang.reflect.Type param0, java.lang.Object param1) {
    return this;
  }

  public com.tasermonkeys.google.json.GsonBuilder setPrettyPrinting() {
    return this;
  }
}