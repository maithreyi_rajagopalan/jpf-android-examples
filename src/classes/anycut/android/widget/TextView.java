package android.widget;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.vm.Abstraction;
import gov.nasa.jpf.vm.AndroidVerify;
import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;

public class TextView extends View {

  @FilterField
  private CharSequence mText = "";
  private BufferType mBufferType;
  private Editable.Factory mEditableFactory = Editable.Factory.getInstance();

  public enum BufferType {
    NORMAL, SPANNABLE, EDITABLE,
  }

  public TextView(Context context) {
    this(context, false);
  }

  public TextView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TextView(Context context, AttributeSet attrs, int defStyle) {
    this(context, false);
  }

  public TextView(Context context, boolean editable) {
    super(context);
    if (editable) {
      setText("", BufferType.EDITABLE);
      mBufferType = BufferType.EDITABLE;
    } else {
      setText("", BufferType.NORMAL);
      mBufferType = BufferType.EDITABLE;
    }

  }

  public final void setText(CharSequence text) {
    mText = text;
    if ((name == null || name.length() == 0) && text != null) {
      name = text.toString();
    }
    if (watcher != null && mText != null) {
      watcher.beforeTextChanged(mText, 0, mText.length(), mText.length());
      watcher.onTextChanged(mText, 0, mText.length(), mText.length());
      watcher.afterTextChanged(((EditText)this).getText());
    }
  }

  public final void setText(int resId) {
    mText = getContext().getString(resId);
    if ((name == null || name.length() == 0) && mText != null) {
      name = mText.toString();
    }
  }

  public CharSequence getText() {
    if (mBufferType.equals(BufferType.EDITABLE)) {
      if (AndroidVerify.getConfigBoolean("verify.symb_input", true)) {
        System.out.println("Getting input for " + this.name);
        String ret = AndroidVerify.getTextInput(this.name);
        System.out.println("Got input " + ret);
        return ret;

      }
    }
    return mText;
  }

  /**
   * Convenience for {@link Selection#getSelectionStart}.
   */
  @ViewDebug.ExportedProperty(category = "text")
  public int getSelectionStart() {
    return Abstraction.TOP_INT;
  }

  /**
   * Convenience for {@link Selection#getSelectionEnd}.
   */
  @ViewDebug.ExportedProperty(category = "text")
  public int getSelectionEnd() {
    return Abstraction.TOP_INT;
  }

  /**
   * Return true iff there is a selection inside this text view.
   */
  public boolean hasSelection() {
    final int selectionStart = getSelectionStart();
    final int selectionEnd = getSelectionEnd();

    return selectionStart >= 0 && selectionStart != selectionEnd;
  }

  private void setText(CharSequence text, BufferType type) {
    if (text == null) {
      text = "";
    }

    if (type == BufferType.EDITABLE) {
      text = mEditableFactory.newEditable(text);
    }

    mBufferType = type;
    mText = text;
  }

  @Override
  public void setLayoutParams(ViewGroup.LayoutParams lp) {

  }

  public void setTextSize(float f) {

  }

  public void setTransformationMethod(TransformationMethod t) {

  }

  public final void setMovementMethod(android.text.method.MovementMethod param0) {
  }

  public void setTypeface(android.graphics.Typeface param0, int param1) {
  }

  protected boolean getDefaultEditable() {
    return Abstraction.TOP_BOOL;
  }

  protected android.text.method.MovementMethod getDefaultMovementMethod() {
    return null;
  }

  // public java.lang.CharSequence getText() {
  // return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  // }

  public int length() {
    return Abstraction.TOP_INT;
  }

  public android.text.Editable getEditableText() {
    return android.text.SpannableStringBuilder.TOP;
  }

  public int getLineHeight() {
    return Abstraction.TOP_INT;
  }

  public final android.text.Layout getLayout() {
    return null;
  }

  public final android.text.method.KeyListener getKeyListener() {
    return null;
  }

  public void setKeyListener(android.text.method.KeyListener param0) {
  }

  public final android.text.method.MovementMethod getMovementMethod() {
    return null;
  }

  // public final void setMovementMethod(android.text.method.MovementMethod param0) {
  // }

  public final android.text.method.TransformationMethod getTransformationMethod() {
    return null;
  }

  // public final void setTransformationMethod(android.text.method.TransformationMethod param0) {
  // }

  public int getCompoundPaddingTop() {
    return Abstraction.TOP_INT;
  }

  public int getCompoundPaddingBottom() {
    return Abstraction.TOP_INT;
  }

  public int getCompoundPaddingLeft() {
    return Abstraction.TOP_INT;
  }

  public int getCompoundPaddingRight() {
    return Abstraction.TOP_INT;
  }

  public int getCompoundPaddingStart() {
    return Abstraction.TOP_INT;
  }

  public int getCompoundPaddingEnd() {
    return Abstraction.TOP_INT;
  }

  public int getExtendedPaddingTop() {
    return Abstraction.TOP_INT;
  }

  public int getExtendedPaddingBottom() {
    return Abstraction.TOP_INT;
  }

  public int getTotalPaddingLeft() {
    return Abstraction.TOP_INT;
  }

  public int getTotalPaddingRight() {
    return Abstraction.TOP_INT;
  }

  public int getTotalPaddingStart() {
    return Abstraction.TOP_INT;
  }

  public int getTotalPaddingEnd() {
    return Abstraction.TOP_INT;
  }

  public int getTotalPaddingTop() {
    return Abstraction.TOP_INT;
  }

  public int getTotalPaddingBottom() {
    return Abstraction.TOP_INT;
  }

  public void setCompoundDrawables(android.graphics.drawable.Drawable param0, android.graphics.drawable.Drawable param1,
                                   android.graphics.drawable.Drawable param2, android.graphics.drawable.Drawable param3) {
  }

  public void setCompoundDrawablesWithIntrinsicBounds(int param0, int param1, int param2, int param3) {
  }

  public void setCompoundDrawablesWithIntrinsicBounds(android.graphics.drawable.Drawable param0, android.graphics.drawable.Drawable param1,
                                                      android.graphics.drawable.Drawable param2, android.graphics.drawable.Drawable param3) {
  }

  public void setCompoundDrawablesRelative(android.graphics.drawable.Drawable param0, android.graphics.drawable.Drawable param1,
                                           android.graphics.drawable.Drawable param2, android.graphics.drawable.Drawable param3) {
  }

  public void setCompoundDrawablesRelativeWithIntrinsicBounds(int param0, int param1, int param2, int param3) {
  }

  public void setCompoundDrawablesRelativeWithIntrinsicBounds(android.graphics.drawable.Drawable param0, android.graphics.drawable.Drawable param1,
                                                              android.graphics.drawable.Drawable param2, android.graphics.drawable.Drawable param3) {
  }

  public android.graphics.drawable.Drawable[] getCompoundDrawables() {
    return ((android.graphics.drawable.Drawable[]) Abstraction.randomObject("android.graphics.drawable.Drawable[]"));
  }

  public android.graphics.drawable.Drawable[] getCompoundDrawablesRelative() {
    return ((android.graphics.drawable.Drawable[]) Abstraction.randomObject("android.graphics.drawable.Drawable[]"));
  }

  public void setCompoundDrawablePadding(int param0) {
  }

  public int getCompoundDrawablePadding() {
    return Abstraction.TOP_INT;
  }

  @Override
  public void setPadding(int param0, int param1, int param2, int param3) {
  }

  @Override
  public void setPaddingRelative(int param0, int param1, int param2, int param3) {
  }

  public final int getAutoLinkMask() {
    return Abstraction.TOP_INT;
  }

  public void setTextAppearance(android.content.Context param0, int param1) {
  }

  public java.util.Locale getTextLocale() {
    return ((java.util.Locale) Abstraction.randomObject("java.util.Locale"));
  }

  public void setTextLocale(java.util.Locale param0) {
  }

  public float getTextSize() {
    return Abstraction.TOP_FLOAT;
  }

  // public void setTextSize(float param0) {
  // }

  public void setTextSize(int param0, float param1) {
  }

  public float getTextScaleX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTextScaleX(float param0) {
  }

  public void setTypeface(android.graphics.Typeface param0) {
  }

  public android.graphics.Typeface getTypeface() {
    return null;
  }

  public void setElegantTextHeight(boolean param0) {
  }

  public float getLetterSpacing() {
    return Abstraction.TOP_FLOAT;
  }

  public void setLetterSpacing(float param0) {
  }

  public java.lang.String getFontFeatureSettings() {
    return Abstraction.TOP_STRING;
  }

  public void setFontFeatureSettings(java.lang.String param0) {
  }

  public void setTextColor(int param0) {
  }

  public void setTextColor(android.content.res.ColorStateList param0) {
  }

  public final android.content.res.ColorStateList getTextColors() {
    return ((android.content.res.ColorStateList) Abstraction.randomObject("android.content.res.ColorStateList"));
  }

  public final int getCurrentTextColor() {
    return Abstraction.TOP_INT;
  }

  public void setHighlightColor(int param0) {
  }

  public int getHighlightColor() {
    return Abstraction.TOP_INT;
  }

  public final void setShowSoftInputOnFocus(boolean param0) {
  }

  public final boolean getShowSoftInputOnFocus() {
    return Abstraction.TOP_BOOL;
  }

  public void setShadowLayer(float param0, float param1, float param2, int param3) {
  }

  public float getShadowRadius() {
    return Abstraction.TOP_FLOAT;
  }

  public float getShadowDx() {
    return Abstraction.TOP_FLOAT;
  }

  public float getShadowDy() {
    return Abstraction.TOP_FLOAT;
  }

  public int getShadowColor() {
    return Abstraction.TOP_INT;
  }

  public android.text.TextPaint getPaint() {
    return null;
  }

  public final void setAutoLinkMask(int param0) {
  }

  public final void setLinksClickable(boolean param0) {
  }

  public final boolean getLinksClickable() {
    return Abstraction.TOP_BOOL;
  }

  public android.text.style.URLSpan[] getUrls() {
    return ((android.text.style.URLSpan[]) Abstraction.randomObject("android.text.style.URLSpan[]"));
  }

  public final void setHintTextColor(int param0) {
  }

  public final void setHintTextColor(android.content.res.ColorStateList param0) {
  }

  public final android.content.res.ColorStateList getHintTextColors() {
    return ((android.content.res.ColorStateList) Abstraction.randomObject("android.content.res.ColorStateList"));
  }

  public final int getCurrentHintTextColor() {
    return Abstraction.TOP_INT;
  }

  public final void setLinkTextColor(int param0) {
  }

  public final void setLinkTextColor(android.content.res.ColorStateList param0) {
  }

  public final android.content.res.ColorStateList getLinkTextColors() {
    return ((android.content.res.ColorStateList) Abstraction.randomObject("android.content.res.ColorStateList"));
  }

  public void setGravity(int param0) {
  }

  public int getGravity() {
    return Abstraction.TOP_INT;
  }

  public int getPaintFlags() {
    return Abstraction.TOP_INT;
  }

  public void setPaintFlags(int param0) {
  }

  public void setHorizontallyScrolling(boolean param0) {
  }

  public void setMinLines(int param0) {
  }

  public int getMinLines() {
    return Abstraction.TOP_INT;
  }

  public void setMinHeight(int param0) {
  }

  public int getMinHeight() {
    return Abstraction.TOP_INT;
  }

  public void setMaxLines(int param0) {
  }

  public int getMaxLines() {
    return Abstraction.TOP_INT;
  }

  public void setMaxHeight(int param0) {
  }

  public int getMaxHeight() {
    return Abstraction.TOP_INT;
  }

  public void setLines(int param0) {
  }

  public void setHeight(int param0) {
  }

  public void setMinEms(int param0) {
  }

  public int getMinEms() {
    return Abstraction.TOP_INT;
  }

  public void setMinWidth(int param0) {
  }

  public int getMinWidth() {
    return Abstraction.TOP_INT;
  }

  public void setMaxEms(int param0) {
  }

  public int getMaxEms() {
    return Abstraction.TOP_INT;
  }

  public void setMaxWidth(int param0) {
  }

  public int getMaxWidth() {
    return Abstraction.TOP_INT;
  }

  public void setEms(int param0) {
  }

  public void setWidth(int param0) {
  }

  public void setLineSpacing(float param0, float param1) {
  }

  public float getLineSpacingMultiplier() {
    return Abstraction.TOP_FLOAT;
  }

  public float getLineSpacingExtra() {
    return Abstraction.TOP_FLOAT;
  }

  public final void append(java.lang.CharSequence param0) {
  }

  public void append(java.lang.CharSequence param0, int param1, int param2) {
  }

  @Override
  protected void drawableStateChanged() {
  }

  @Override
  public void drawableHotspotChanged(float param0, float param1) {
  }

  @Override
  public android.os.Parcelable onSaveInstanceState() {
    return null;
  }

  @Override
  public void onRestoreInstanceState(android.os.Parcelable param0) {
  }

  public void setFreezesText(boolean param0) {
  }

  public boolean getFreezesText() {
    return Abstraction.TOP_BOOL;
  }

  public final void setEditableFactory(android.text.Editable.Factory param0) {
  }

  public final void setSpannableFactory(android.text.Spannable.Factory param0) {
  }

  // public final void setText(java.lang.CharSequence param0){
  // }

  public final void setTextKeepState(java.lang.CharSequence param0) {
  }

  // public void setText(java.lang.CharSequence param0, android.widget.TextView.BufferType param1){
  // }

  public final void setText(char[] param0, int param1, int param2) {
  }

  public final void setTextKeepState(java.lang.CharSequence param0, android.widget.TextView.BufferType param1) {
  }

  // public final void setText(int param0){
  // }

  public final void setText(int param0, android.widget.TextView.BufferType param1) {
  }

  public final void setHint(java.lang.CharSequence param0) {
  }

  public final void setHint(int param0) {
  }

  public java.lang.CharSequence getHint() {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public void setInputType(int param0) {
  }

  public void setRawInputType(int param0) {
  }

  public int getInputType() {
    return Abstraction.TOP_INT;
  }

  public void setImeOptions(int param0) {
  }

  public int getImeOptions() {
    return Abstraction.TOP_INT;
  }

  public void setImeActionLabel(java.lang.CharSequence param0, int param1) {
  }

  public java.lang.CharSequence getImeActionLabel() {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public int getImeActionId() {
    return Abstraction.TOP_INT;
  }

  // public void setOnEditorActionListener(android.widget.TextView.OnEditorActionListener param0){
  // }

  public void onEditorAction(int param0) {
  }

  public void setPrivateImeOptions(java.lang.String param0) {
  }

  public java.lang.String getPrivateImeOptions() {
    return Abstraction.TOP_STRING;
  }

  public void setInputExtras(int param0) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
  }

  public android.os.Bundle getInputExtras(boolean param0) {
    return null;
  }

  public java.lang.CharSequence getError() {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public void setError(java.lang.CharSequence param0) {
  }

  public void setError(java.lang.CharSequence param0, android.graphics.drawable.Drawable param1) {
  }

  protected boolean setFrame(int param0, int param1, int param2, int param3) {
    return Abstraction.TOP_BOOL;
  }

  public void setFilters(android.text.InputFilter[] param0) {
  }

  public android.text.InputFilter[] getFilters() {
    return ((android.text.InputFilter[]) Abstraction.randomObject("android.text.InputFilter[]"));
  }

  public boolean onPreDraw() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  protected void onAttachedToWindow() {
  }

  @Override
  public void onScreenStateChanged(int param0) {
  }

  @Override
  protected boolean isPaddingOffsetRequired() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  protected int getLeftPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected int getTopPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected int getBottomPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected int getRightPaddingOffset() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected boolean verifyDrawable(android.graphics.drawable.Drawable param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void jumpDrawablesToCurrentState() {
  }

  @Override
  public void invalidateDrawable(android.graphics.drawable.Drawable param0) {
  }

  @Override
  public boolean hasOverlappingRendering() {
    return Abstraction.TOP_BOOL;
  }

  public boolean isTextSelectable() {
    return Abstraction.TOP_BOOL;
  }

  public void setTextIsSelectable(boolean param0) {
  }

  @Override
  protected int[] onCreateDrawableState(int param0) {
    return ((int[]) Abstraction.randomObject("int[]"));
  }

  @Override
  protected void onDraw(android.graphics.Canvas param0) {
  }

  @Override
  public void getFocusedRect(android.graphics.Rect param0) {
  }

  public int getLineCount() {
    return Abstraction.TOP_INT;
  }

  public int getLineBounds(int param0, android.graphics.Rect param1) {
    return Abstraction.TOP_INT;
  }

  @Override
  public int getBaseline() {
    return Abstraction.TOP_INT;
  }

  @Override
  public boolean onKeyPreIme(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onKeyDown(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onKeyMultiple(int param0, int param1, android.view.KeyEvent param2) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onKeyUp(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onCheckIsTextEditor() {
    return Abstraction.TOP_BOOL;
  }

  public android.view.inputmethod.InputConnection onCreateInputConnection(android.view.inputmethod.EditorInfo param0) {
    return null;
  }

  public boolean extractText(android.view.inputmethod.ExtractedTextRequest param0, android.view.inputmethod.ExtractedText param1) {
    return Abstraction.TOP_BOOL;
  }

  public void setExtractedText(android.view.inputmethod.ExtractedText param0) {
  }

  public void onCommitCompletion(android.view.inputmethod.CompletionInfo param0) {
  }

  public void onCommitCorrection(android.view.inputmethod.CorrectionInfo param0) {
  }

  public void beginBatchEdit() {
  }

  public void endBatchEdit() {
  }

  public void onBeginBatchEdit() {
  }

  public void onEndBatchEdit() {
  }

  public boolean onPrivateIMECommand(java.lang.String param0, android.os.Bundle param1) {
    return Abstraction.TOP_BOOL;
  }

  public void setIncludeFontPadding(boolean param0) {
  }

  public boolean getIncludeFontPadding() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  protected void onMeasure(int param0, int param1) {
  }

  @Override
  protected void onLayout(boolean param0, int param1, int param2, int param3, int param4) {
  }

  public boolean bringPointIntoView(int param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean moveCursorToVisibleOffset() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void computeScroll() {
  }

  public void debug(int param0) {
  }

  public void setSingleLine() {
  }

  public void setAllCaps(boolean param0) {
  }

  public void setSingleLine(boolean param0) {
  }

//  public void setEllipsize(android.text.TextUtils.TruncateAt param0) {
//  }

  public void setMarqueeRepeatLimit(int param0) {
  }

  public int getMarqueeRepeatLimit() {
    return Abstraction.TOP_INT;
  }

//  public android.text.TextUtils.TruncateAt getEllipsize() {
//    return ((android.text.TextUtils.TruncateAt) Abstraction.randomObject("android.text.TextUtils.TruncateAt"));
//  }

  public void setSelectAllOnFocus(boolean param0) {
  }

  public void setCursorVisible(boolean param0) {
  }

  public boolean isCursorVisible() {
    return Abstraction.TOP_BOOL;
  }

  protected void onTextChanged(java.lang.CharSequence param0, int param1, int param2, int param3) {
  }

  protected void onSelectionChanged(int param0, int param1) {
  }

  android.text.TextWatcher watcher;

  public void addTextChangedListener(android.text.TextWatcher param0) {
    this.watcher = param0;
  }

  public void removeTextChangedListener(android.text.TextWatcher param0) {
  }

  @Override
  public void onStartTemporaryDetach() {
  }

  @Override
  public void onFinishTemporaryDetach() {
  }

  @Override
  protected void onFocusChanged(boolean param0, int param1, android.graphics.Rect param2) {
  }

  @Override
  public void onWindowFocusChanged(boolean param0) {
  }

  @Override
  protected void onVisibilityChanged(android.view.View param0, int param1) {
  }

  public void clearComposingText() {
  }

  @Override
  public void setSelected(boolean param0) {
  }

  @Override
  public boolean onTouchEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public boolean onGenericMotionEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean didTouchFocusSelect() {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void cancelLongPress() {
  }

  @Override
  public boolean onTrackballEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  public void setScroller(android.widget.Scroller param0) {
  }

  @Override
  protected float getLeftFadingEdgeStrength() {
    return Abstraction.TOP_FLOAT;
  }

  @Override
  protected float getRightFadingEdgeStrength() {
    return Abstraction.TOP_FLOAT;
  }

  @Override
  protected int computeHorizontalScrollRange() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected int computeVerticalScrollRange() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected int computeVerticalScrollExtent() {
    return Abstraction.TOP_INT;
  }

  @Override
  public void findViewsWithText(java.util.ArrayList param0, java.lang.CharSequence param1, int param2) {
  }

  @Override
  public boolean onKeyShortcut(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void onPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  @Override
  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  @Override
  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }

  @Override
  public boolean performAccessibilityAction(int param0, android.os.Bundle param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void sendAccessibilityEvent(int param0) {
  }

  public boolean isInputMethodTarget() {
    return Abstraction.TOP_BOOL;
  }

  public boolean onTextContextMenuItem(int param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  protected void onScrollChanged(int param0, int param1, int param2, int param3) {
  }

  public boolean isSuggestionsEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setCustomSelectionActionModeCallback(android.view.ActionMode.Callback param0) {
  }

  public android.view.ActionMode.Callback getCustomSelectionActionModeCallback() {
    return ((android.view.ActionMode.Callback) Abstraction.randomObject("android.view.ActionMode.Callback"));
  }

  public int getOffsetForPosition(float param0, float param1) {
    return Abstraction.TOP_INT;
  }

  @Override
  public boolean onDragEvent(android.view.DragEvent param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void onRtlPropertiesChanged(int param0) {
  }

  @Override
  public boolean processEvent(Event event) {
    // TODO Auto-generated method stub
    return super.processEvent(event);
  }

}