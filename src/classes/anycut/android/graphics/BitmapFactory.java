package android.graphics;

import gov.nasa.jpf.vm.AndroidVerify;

import java.io.InputStream;

public class BitmapFactory {
  public static android.graphics.BitmapFactory TOP = new android.graphics.BitmapFactory();

  public static class Options {

  }

  public BitmapFactory() {
  }

  public static Bitmap decodeStream(InputStream is, Rect outPadding, Options opts) {
    return (Bitmap) AndroidVerify.getValues(new Object[] { null, android.graphics.Bitmap.TOP }, "BitmapFactory.decodeResource");
  }

  public static android.graphics.Bitmap decodeResource(android.content.res.Resources param0, int param1) {
    return android.graphics.Bitmap.TOP;
  }

  public static Bitmap decodeByteArray(byte[] data, int offset, int length) {
    return android.graphics.Bitmap.TOP;
  }

  public static Bitmap decodeStream(InputStream is) {
    return Bitmap.TOP;
  }
}