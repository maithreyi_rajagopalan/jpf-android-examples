package agrep;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_org_mozilla_universalchardet_UniversalDetector extends NativePeer{
  @MJI
  public static long chardet_create(MJIEnv env, int objref) {
    return 1;

  }

  @MJI
  public static void chardet_destroy(MJIEnv env, int objref, long det) {
  }

  @MJI
  public static int chardet_handle_data(MJIEnv env, int objref, long det, byte[] data, int offset, int len) {
    return 0;

  }

  @MJI
  public static int chardet_data_end(MJIEnv env, int objref, long det) {
    return 0;

  }

  @MJI
  public static int chardet_reset(MJIEnv env, int objref, long det) {
    return 0;

  }

  @MJI
  public static int chardet_get_charset(MJIEnv env, int objref, long det) {
    return env.newString("UTF-8");
  }

}
