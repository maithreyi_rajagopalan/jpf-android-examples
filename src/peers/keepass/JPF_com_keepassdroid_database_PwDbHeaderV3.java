package keepass;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_com_keepassdroid_database_PwDbHeaderV3 extends NativePeer {

  @MJI
  public static boolean matchesHeader(MJIEnv env, int clsref, int sig1, int sig2) {
    return true;
  }

  //
  // @MJI
  // public boolean matchesVersion(MJIEnv env, int clsref) {
  // return true;
  // }
  //
  // @MJI
  // public static boolean compatibleHeaders(MJIEnv env, int clsref, int one, int two) {
  // return true;
  // }

  @MJI
  public void loadFromFile(MJIEnv env, int objRef, int bytes, int offset) {
    // signature1 = LEDataInputStream.readInt( buf, offset + 0 );
    // signature2 = LEDataInputStream.readInt( buf, offset + 4 );
    // flags = LEDataInputStream.readInt( buf, offset + 8 );
    // version = LEDataInputStream.readInt( buf, offset + 12 );
    //
    // System.arraycopy( buf, offset + 16, masterSeed, 0, 16 );
    // System.arraycopy( buf, offset + 32, encryptionIV, 0, 16 );
    //
    // numGroups = LEDataInputStream.readInt( buf, offset + 48 );
    // numEntries = LEDataInputStream.readInt( buf, offset + 52 );
    //
    // System.arraycopy( buf, offset + 56, contentsHash, 0, 32 );
    //
    // System.arraycopy( buf, offset + 88, transformSeed, 0, 32 );
    // numKeyEncRounds = LED

    env.setIntField(objRef, "signature1",
        env.getStaticIntField("com.keepassdroid.database.PwDbHeader", "PWM_DBSIG_1"));
    env.setIntField(objRef, "signature2", env.getStaticIntField(env.getClassInfo(objRef), "DBSIG_2"));
    env.setIntField(objRef, "version", env.getStaticIntField(env.getClassInfo(objRef), "DBVER_DW"));

    // env.setReferenceField(objRef, "masterSeed", env.newByteArray(16));
    // env.setReferenceField(objRef, "encryptionIV", env.newByteArray(16));

    env.setIntField(objRef, "numGroups", 0);
    env.setIntField(objRef, "numEntries", 0);

    // env.setReferenceField(objRef, "transformSeed", env.newByteArray(32));
    env.setIntField(objRef, "numKeyEncRounds", 1);
    env.setIntField(objRef, "flags", env.getStaticIntField(env.getClassInfo(objRef), "FLAG_RIJNDAEL"));

  }
}
