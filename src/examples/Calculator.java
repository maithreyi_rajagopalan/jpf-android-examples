import java.io.IOException;

public class Calculator {

  public static void main(String[] args) throws IOException {
    int SEARCH_DEPTH = 1000;
    int EVENT_DEPTH = 20 ;
    String EVENT_STRATEGY = "heuristic";

    String[] config_properties = { "-show", "src/examples/TestCalc.jpf",
        "+search.depth_limit=" + SEARCH_DEPTH, "+event.max_depth=" + EVENT_DEPTH,
        "+event.strategy=" + EVENT_STRATEGY };

    RunJPFAndroid run = new RunJPFAndroid("calculator", config_properties, args);
    run.runJPF();
  }

}
